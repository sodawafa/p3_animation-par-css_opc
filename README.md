# Projet Ohmyfood

> Projet n ° 3 dans le cadre de ma formation de développeur Front-End avec Openclassrooms

## URL

###Lien projet : 
[Ohmyfood](https://sodawafa.gitlab.io/p3_animation-par-css_opc/), 
[Menu 1](https://sodawafa.gitlab.io/p3_animation-par-css_opc/Menu1.html),
[Menu 2](https://sodawafa.gitlab.io/p3_animation-par-css_opc/Menu2.html),
[Menu 3](https://sodawafa.gitlab.io/p3_animation-par-css_opc/Menu3.html),
[Menu 4](https://sodawafa.gitlab.io/p3_animation-par-css_opc/Menu4.html).

###Lien projet sur **GIT** : [Ohmyfood](https://gitlab.com/sodawafa/p3_animation-par-css_opc)

###Validator-W3C :
Html Checker :
[Index](https://validator.w3.org/nu/?doc=https%3A%2F%2Fsodawafa.gitlab.io%2Fp3_animation-par-css_opc),
[Menu 1](https://validator.w3.org/nu/?doc=https%3A%2F%2Fsodawafa.gitlab.io%2Fp3_animation-par-css_opc%2FMenu1.html),
[Menu 2](https://validator.w3.org/nu/?doc=https%3A%2F%2Fsodawafa.gitlab.io%2Fp3_animation-par-css_opc%2FMenu2.html),
[Menu 3](https://validator.w3.org/nu/?doc=https%3A%2F%2Fsodawafa.gitlab.io%2Fp3_animation-par-css_opc%2FMenu3.html),
[Menu 4](https://validator.w3.org/nu/?doc=https%3A%2F%2Fsodawafa.gitlab.io%2Fp3_animation-par-css_opc%2FMenu4.html).

CSS Checker :
[Style-Index](https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fsodawafa.gitlab.io%2Fp3_animation-par-css_opc%2Fcss%2Fmain.css&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr),
[Style-Menu](https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fsodawafa.gitlab.io%2Fp3_animation-par-css_opc%2Fcss%2Fmain1.css&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr)
## Technologies

>HTML5, SASS

## IDE

[PhpStorm](https://www.jetbrains.com/fr-fr/phpstorm/)

## Auteurs

[Wafa SODA](https://gitlab.com/sodawafa)
<[sodawafa@gmail.com](mailto:sodawafa@gmail.com)>




